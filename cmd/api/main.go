package main

import (
	"bankingapp/internal/api/delivery"
	"bankingapp/internal/api/repository"
	"bankingapp/internal/api/usecase"
	"time"

	"database/sql"
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

const (
	host     = "localhost"
	port     = 5432
	usr      = "postgres"
	password = "postgres"
	dbname   = "banking_app"
)

func dbInit() *sql.DB {
	// основные настройки к базе

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, usr, password, dbname)

	dbUsers, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	dbUsers.SetMaxOpenConns(10)

	err = dbUsers.Ping() // вот тут будет первое подключение к базе
	if err != nil {
		dbUsers.Ping()
		time.Sleep(5 * time.Second)
	}
	return dbUsers
}

func main() {
	dbUsers := dbInit()
	zapLogger, err := zap.NewProduction()
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		error := zapLogger.Sync()
		if error != nil {
			fmt.Println(error)
		}
	}()

	logger := zapLogger.Sugar()

	usersRepo := repository.NewRepo(dbUsers)
	users := usecase.NewUserUseCase(usersRepo)
	userHandler := delivery.UserHandler{
		UserUseCase: users,
		Logger:      logger,
	}

	handlersMux := delivery.RouterWithHandlers(userHandler)
	middlewareMux := delivery.PostProcessingRouter(handlersMux, logger)
	addr := ":8080"
	logger.Infow("starting api",
		"type", "START",
		"addr", addr,
	)
	err = http.ListenAndServe(addr, middlewareMux)
	if err != nil {
		logger.Error("Fail ListenAndServe with err: %v", err)
	}
}
