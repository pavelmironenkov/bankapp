package main

import (
	"bankingapp/internal/producer/delivery"
	"bankingapp/internal/producer/usecase"
	"fmt"
	"net/http"

	kafka "github.com/IBM/sarama"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

func main() {
	zapLogger, err := zap.NewProduction()
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		error := zapLogger.Sync()
		if error != nil {
			fmt.Println(error)
		}
	}()

	logger := zapLogger.Sugar()
	config := kafka.NewConfig()
	config.Producer.Return = struct {
		Successes bool
		Errors    bool
	}{true, true}

	p, err := kafka.NewSyncProducer([]string{"127.0.0.1:29092"}, nil)
	if err != nil {
		panic(err)
	}
	defer p.Close()

	transUseCase := usecase.NewTransactionsUseCase(p)
	transHandler := delivery.TransactionsHandler{TransUseCase: transUseCase, Logger: logger}
	handlersMux := delivery.RouterWithHandlers(transHandler)
	middlewareMux := delivery.PostProcessingRouter(handlersMux, logger)
	addr := ":8081"
	logger.Infow("starting producer",
		"type", "START",
		"addr", addr,
	)
	err = http.ListenAndServe(addr, middlewareMux)
	if err != nil {
		logger.Error("Fail ListenAndServe with err: %v", err)
	}
}
