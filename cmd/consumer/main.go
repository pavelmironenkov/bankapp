package main

import (
	"bankingapp/internal/consumer/repository"
	"bankingapp/internal/consumer/usecase"
	"context"
	"log"
	"time"

	"database/sql"
	"fmt"

	kafka "github.com/IBM/sarama"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

const (
	host     = "localhost"
	port     = 5432
	usr      = "postgres"
	password = "postgres"
	dbname   = "bankingapp"
)

func dbInit() *sql.DB {
	// основные настройки к базе

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, usr, password, dbname)

	dbTransaction, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	dbTransaction.SetMaxOpenConns(10)

	err = dbTransaction.Ping() // вот тут будет первое подключение к базе
	if err != nil {
		dbTransaction.Ping()
		time.Sleep(5 * time.Second)
	}
	return dbTransaction
}

func main() {
	dbTransaction := dbInit()
	zapLogger, err := zap.NewProduction()
	if err != nil {
		fmt.Println(err)
	}
	defer func() {
		error := zapLogger.Sync()
		if error != nil {
			fmt.Println(error)
		}
	}()

	logger := zapLogger.Sugar()
	consumer, err := kafka.NewConsumer([]string{"127.0.0.1:29092"}, nil)
	if err != nil {
		log.Fatalf("Failed to create consumer: %v", err)
	}
	defer consumer.Close()
	repo := repository.NewRepo(dbTransaction)
	transactions := usecase.Transactions{Consumer: consumer, Repo: repo, Logger: logger}
	err = transactions.ConsumeTransaction(context.Background())
	if err != nil {
		logger.Infof("Error at update balance: %v", err)
	}
}
