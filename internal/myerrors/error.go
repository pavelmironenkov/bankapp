package myerrors

import (
	"encoding/json"
	"errors"
	"net/http"
)

var (
	ErrExistUser        = errors.New("already exist")
	ErrNoUser           = errors.New("user not found")
	ErrNoTransaction    = errors.New("transaction not found")
	ErrBadPass          = errors.New("invalid password")
	ErrBadDBTransaction = errors.New("can't create db transaction")
	ErrNoAuth           = errors.New("user havn't authentificate")
)

func JsonError(w http.ResponseWriter, status int, msg string) {
	resp, err := json.Marshal(map[string]interface{}{
		"status": status,
		"error":  msg,
	})
	HasntMarshalError(w, err, resp)
}

func HasntMarshalError(w http.ResponseWriter, err error, resp []byte) bool {
	if err != nil {
		http.Error(w, "Marshalling err", http.StatusInternalServerError)
		return false
	}
	_, err = w.Write(resp)
	if err != nil {
		http.Error(w, "Writing response err", http.StatusInternalServerError)
		return false
	}
	return true
}
