package middleware

import (
	"net/http"

	"bankingapp/pkg/token"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		if tokenString == ""{
			next.ServeHTTP(w, r)
			return 
		}
		user, err := token.Check(tokenString)
		if user != nil && err == nil {
			ctx := token.ContextWithUser(r.Context(), user)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
