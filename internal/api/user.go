package user

type User struct {
	ID    string `json:"id"`
	Email string `json:"email"`
	Name  string `json:"name"`
}

type Account struct {
	ID       string  `json:"id"`
	Username string  `json:"username"`
	Balance  float64 `json:"balance"`
	Password string  `json:"-"`
	UserID   string  `json:"userid"`
}

