package repository

import (
	user "bankingapp/internal/api"
	"bankingapp/internal/myerrors"
	trans "bankingapp/internal/producer"
	"context"
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type UserRepo interface {
	Authorize(ctx context.Context, username, pass string) (user.Account, error)
	Register(ctx context.Context, username, pass, name, email string) (user.Account, error)
	GetPersonalInfo(ctx context.Context, userID string) (user.User, error)
	GetAccountInfo(ctx context.Context, accoundID string) (user.Account, error)
	GetTransaction(ctx context.Context, transactionID string) (trans.Transaction, error)
	GetHistoryTransactions(ctx context.Context, acccountID string) ([]trans.Transaction, error)
}

type UserRepository struct {
	dbUsers *sql.DB
}

func NewRepo(db *sql.DB) *UserRepository {
	return &UserRepository{
		dbUsers: db,
	}
}

func (repo *UserRepository) Authorize(ctx context.Context, username, pass string) (user.Account, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return user.Account{}, myerrors.ErrBadDBTransaction
	}
	account := user.Account{Username: username}
	row := tr.QueryRowContext(ctx, "SELECT accountID, password, userID FROM account WHERE username = $1", username)
	err = row.Scan(&account.ID, &account.Password, &account.UserID)
	if err == sql.ErrNoRows {
		return user.Account{}, myerrors.ErrNoUser
	}
	if account.Password != pass {
		return user.Account{}, myerrors.ErrBadPass
	}
	tr.Commit()
	return account, nil
}

func (repo *UserRepository) Register(ctx context.Context, username, pass, name, email string) (user.Account, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{})
	defer tr.Rollback()
	if err != nil {
		return user.Account{}, fmt.Errorf("DB error: can't create transaction")
	}
	account := user.Account{Username: username}
	row := tr.QueryRowContext(ctx, "SELECT accountID FROM account WHERE username = $1", username)
	var id []byte
	err = row.Scan(&id)
	if err != sql.ErrNoRows {
		fmt.Println(err)
		return user.Account{}, myerrors.ErrExistUser
	}
	row = tr.QueryRowContext(ctx, "INSERT INTO usr (email, name) VALUES ($1, $2) RETURNING userID", email, name)
	err = row.Scan(&id)
	if err != nil {
		return user.Account{}, errors.Wrap(err, "DB error: can't create new user")
	}
	fmt.Println("userID", id, err)
	account.UserID = string(id)
	row = tr.QueryRowContext(ctx, "INSERT INTO account (username, password, userID) VALUES ($1, $2, $3) RETURNING accountID", username, pass, id)
	err = row.Scan(&id)
	if err != nil {
		return user.Account{}, errors.Wrap(err, "DB error: can't create new account")
	}
	fmt.Println("accountID", id, err)
	account.ID = string(id)
	tr.Commit()
	return account, nil
}

func (repo *UserRepository) GetPersonalInfo(ctx context.Context, userID string) (user.User, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return user.User{}, myerrors.ErrBadDBTransaction
	}
	u := user.User{ID: userID}
	row := tr.QueryRowContext(ctx, "SELECT email, name FROM usr WHERE userID = $1", userID)
	err = row.Scan(&u.Email, &u.Name)
	if err == sql.ErrNoRows {
		return user.User{}, myerrors.ErrNoUser
	}
	tr.Commit()
	return u, nil

}
func (repo *UserRepository) GetAccountInfo(ctx context.Context, AccoundID string) (user.Account, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return user.Account{}, myerrors.ErrBadDBTransaction
	}
	a := user.Account{ID: AccoundID}
	row := tr.QueryRowContext(ctx, "SELECT balance, username, password, userID FROM account WHERE accountID = $1", AccoundID)
	err = row.Scan(&a.Balance, &a.Username, &a.Password, &a.UserID)
	if err == sql.ErrNoRows {
		return user.Account{}, myerrors.ErrNoUser
	}
	tr.Commit()
	return a, nil
}

func (repo *UserRepository) GetTransaction(ctx context.Context, transactionID string) (trans.Transaction, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return trans.Transaction{}, myerrors.ErrBadDBTransaction
	}
	t := trans.Transaction{TransactionID: transactionID}
	row := tr.QueryRowContext(ctx, "SELECT amount, timestamp, accountID FROM transaction WHERE transactionID = $1", transactionID)
	err = row.Scan(&t.Amount, &t.Timestamp, &t.AccountID)
	if err == sql.ErrNoRows {
		return trans.Transaction{}, myerrors.ErrNoTransaction
	}
	tr.Commit()
	return t, nil
}

func (repo *UserRepository) GetHistoryTransactions(ctx context.Context, accountID string) ([]trans.Transaction, error) {
	tr, err := repo.dbUsers.BeginTx(ctx, &sql.TxOptions{ReadOnly: true})
	defer tr.Rollback()
	if err != nil {
		return []trans.Transaction{}, myerrors.ErrBadDBTransaction
	}
	var transactions []trans.Transaction
	rows, err := tr.QueryContext(ctx, "SELECT amount, timestamp, accountID FROM transaction WHERE acconuntID = $1", accountID)
	if err != nil {
		return []trans.Transaction{}, err
	}
	defer rows.Close()
	for rows.Next() {
		t := trans.Transaction{AccountID: accountID}
		if err = rows.Scan(&t.Amount, &t.Timestamp, &t.AccountID); err != nil {
			return []trans.Transaction{}, myerrors.ErrNoTransaction
		}
		transactions = append(transactions, t)
	}
	tr.Commit()
	return transactions, nil
}
