package delivery

import (
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"bankingapp/internal/middleware"
)

func RouterWithHandlers(userHandler UserHandler) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/api/register", userHandler.Registrate).Methods("POST")
	r.HandleFunc("/api/login", userHandler.Login).Methods("POST")
	r.HandleFunc("/api/personalinfo", userHandler.GetPersonalInfo).Methods("GET")
	r.HandleFunc("/api/accountinfo", userHandler.GetAccountInfo).Methods("GET")
	r.HandleFunc("/api/transaction/{transactionID}", userHandler.GetTransaction).Methods("GET")
	r.HandleFunc("/api/transactions", userHandler.GetHistoryTransactions).Methods("GET")
	return r
}

func PostProcessingRouter(r *mux.Router, logger *zap.SugaredLogger) http.Handler {
	r.Use(middleware.Auth)
	r.Use(middleware.AccessLog(logger))
	r.Use(middleware.Panic)
	return r
}
