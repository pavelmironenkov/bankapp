package delivery

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	user "bankingapp/internal/api"
	"bankingapp/internal/api/usecase"
	"bankingapp/internal/myerrors"
)

type RegistrateData struct {
	Username string `json:"Username"`
	Password string `json:"Password"`
	Email    string `json:"Email"`
	Name     string `json:"Name"`
}

type UserHandler struct {
	Logger      *zap.SugaredLogger
	UserUseCase usecase.UserUseCase
}

func (h *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		myerrors.JsonError(w, http.StatusBadRequest, "unknown payload")
		return
	}
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant read request body")
		return
	}
	fd := &user.Account{}
	err = json.Unmarshal(body, fd)
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant unpack payload")
		return
	}
	tokenString, err := h.UserUseCase.Authorize(r.Context(), fd.Username, fd.Password)
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, error := json.Marshal(map[string]interface{}{
		"token":       tokenString,
		"Status code": http.StatusOK,
	})
	myerrors.HasntMarshalError(w, error, resp)
	h.Logger.Infof("Send token: %v on client for logining user", tokenString)
}

func (h *UserHandler) Registrate(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		myerrors.JsonError(w, http.StatusBadRequest, "unknown payload")
		return
	}
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant read request body")
		return
	}
	fd := &RegistrateData{}
	err = json.Unmarshal(body, fd)
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant unpack payload")
		return
	}
	tokenString, err := h.UserUseCase.Register(r.Context(), fd.Username, fd.Password, fd.Name, fd.Email)
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, error := json.Marshal(map[string]interface{}{
		"token":       tokenString,
		"Status code": http.StatusOK,
	})
	myerrors.HasntMarshalError(w, error, resp)
	h.Logger.Infof("Send token: %v on client for registarting user", tokenString)
}

func (h *UserHandler) GetPersonalInfo(w http.ResponseWriter, r *http.Request) {
	user, err := h.UserUseCase.GetPersonalInfo(r.Context())
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, err := json.Marshal(user)
	ok := myerrors.HasntMarshalError(w, err, resp)
	if ok {
		h.Logger.Infof("Send user with id: %v ", user.ID)
	}
}

func (h *UserHandler) GetAccountInfo(w http.ResponseWriter, r *http.Request) {
	account, err := h.UserUseCase.GetAccountInfo(r.Context())
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, err := json.Marshal(account)
	ok := myerrors.HasntMarshalError(w, err, resp)
	if ok {
		h.Logger.Infof("Send account with id: %v ", account.ID)
	}
}

func (h *UserHandler) GetTransaction(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	transactionID, ok := vars["transactionID"]
	if !ok {
		myerrors.JsonError(w, http.StatusBadRequest, "hasn't transactionID at vars")
	}
	transaction, err := h.UserUseCase.GetTransaction(r.Context(), transactionID)
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, err := json.Marshal(transaction)
	ok = myerrors.HasntMarshalError(w, err, resp)
	if ok {
		h.Logger.Infof("Send transaction with id: %v ", transactionID)
	}
}

func (h *UserHandler) GetHistoryTransactions(w http.ResponseWriter, r *http.Request) {
	transactions, err := h.UserUseCase.GetHistoryTransactions(r.Context())
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	resp, err := json.Marshal(transactions)
	ok := myerrors.HasntMarshalError(w, err, resp)
	if ok && len(transactions) > 0 {
		h.Logger.Infof("Send transactions for account with id: %v ", transactions[0].AccountID)
	}
}
