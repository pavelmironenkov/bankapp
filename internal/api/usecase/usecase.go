package usecase

import (
	user "bankingapp/internal/api"
	"bankingapp/internal/api/repository"
	trans "bankingapp/internal/producer"
	"bankingapp/pkg/token"
	"context"
	"fmt"
)

type UserUseCase interface {
	Authorize(ctx context.Context, username, pass string) (string, error)
	Register(ctx context.Context, username, pass, name, email string) (string, error)
	GetPersonalInfo(ctx context.Context) (user.User, error)
	GetAccountInfo(ctx context.Context) (user.Account, error)
	GetTransaction(ctx context.Context, transactionID string) (trans.Transaction, error)
	GetHistoryTransactions(ctx context.Context) ([]trans.Transaction, error)
}

type userUseCase struct {
	repo repository.UserRepo
}

func NewUserUseCase(u repository.UserRepo) *userUseCase {
	return &userUseCase{
		repo: u,
	}
}

func (u *userUseCase) Authorize(ctx context.Context, username, pass string) (string, error) {
	account, err := u.repo.Authorize(ctx, username, pass)
	if err != nil {
		return "", err
	}
	return token.Create(token.UserFromToken{UserID: account.UserID, AccountID: account.ID})
}

func (u *userUseCase) Register(ctx context.Context, username, pass, name, email string) (string, error) {
	account, err := u.repo.Register(ctx, username, pass, name, email)
	if err != nil {
		return "", err
	}
	return token.Create(token.UserFromToken{UserID: account.UserID, AccountID: account.ID})
}

func (u *userUseCase) GetPersonalInfo(ctx context.Context) (user.User, error) {
	userFromToken, err := token.UserFromContext(ctx)
	if err != nil {
		return user.User{}, fmt.Errorf("can't read userID from token")
	}
	return u.repo.GetPersonalInfo(ctx, userFromToken.UserID)
}

func (u *userUseCase) GetAccountInfo(ctx context.Context) (user.Account, error) {
	userFromToken, err := token.UserFromContext(ctx)
	if err != nil {
		return user.Account{}, fmt.Errorf("can't read accountID from token")
	}
	return u.repo.GetAccountInfo(ctx, userFromToken.AccountID)
}

func (u *userUseCase) GetTransaction(ctx context.Context, transactionID string) (trans.Transaction, error) {
	userFromToken, err := token.UserFromContext(ctx)
	if err != nil {
		return trans.Transaction{}, fmt.Errorf("can't read accountID from token")
	}
	transaction, err := u.repo.GetTransaction(ctx, transactionID)
	if err != nil {
		return transaction, err
	}
	if transaction.AccountID != userFromToken.AccountID {
		return trans.Transaction{}, fmt.Errorf("you haven't access to this transaction")
	}
	return transaction, err
}

func (u *userUseCase) GetHistoryTransactions(ctx context.Context) ([]trans.Transaction, error) {
	userFromToken, err := token.UserFromContext(ctx)
	if err != nil {
		return []trans.Transaction{}, fmt.Errorf("can't read accountID from token")
	}
	return u.repo.GetHistoryTransactions(ctx, userFromToken.AccountID)
}