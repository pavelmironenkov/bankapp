package usecase

import (
	trans "bankingapp/internal/consumer"
	"bankingapp/internal/consumer/repository"
	"context"
	"encoding/json"
	"log"

	kafka "github.com/IBM/sarama"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type TransactionsUseCase interface {
	ConsumeTransaction(ctx context.Context) error
}

type Transactions struct {
	Logger   *zap.SugaredLogger
	Consumer kafka.Consumer
	Repo     repository.TransactionRepo
}

func (t *Transactions) ConsumeTransaction(ctx context.Context) error {
	partConsumer, err := t.Consumer.ConsumePartition("queue_transactions", 0, kafka.OffsetNewest)
	if err != nil {
		return errors.Wrap(err, "Error at consume partition")
	}
	defer partConsumer.Close()

	for msg := range partConsumer.Messages() {
		var receivedMessage trans.Transaction
		err := json.Unmarshal(msg.Value, &receivedMessage)

		if err != nil {
			log.Printf("Error unmarshaling JSON: %v\n", err)
			continue
		}

		if receivedMessage.Amount > 0.0 {
			go func(receivedMessage trans.Transaction) {
				err = t.Repo.PutMoney(ctx, receivedMessage)
				for err != nil {
					err = t.Repo.PutMoney(ctx, receivedMessage)
				}
			}(receivedMessage)
		} else if receivedMessage.Amount < 0.0 {
			go func(receivedMessage trans.Transaction) {
				err = t.Repo.GetMoney(ctx, receivedMessage)
				for err != nil && err.Error() != "bad balance" {
					err = t.Repo.GetMoney(ctx, receivedMessage)
				}
			}(receivedMessage)
		}
	}
	return nil
}
