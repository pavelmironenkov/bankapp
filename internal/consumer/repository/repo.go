package repository

import (
	trans "bankingapp/internal/consumer"
	"bankingapp/internal/myerrors"
	"context"
	"database/sql"
	"fmt"
	"math"
	"time"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

type TransactionRepo interface {
	GetMoney(ctx context.Context, tr trans.Transaction) error
	PutMoney(ctx context.Context, tr trans.Transaction) error
}

type transactionRepo struct {
	dbTrans *sql.DB
}

func NewRepo(db *sql.DB) *transactionRepo {
	return &transactionRepo{dbTrans: db}
}

func (repo *transactionRepo) GetMoney(ctx context.Context, tr trans.Transaction) error {
	trDB, err := repo.dbTrans.BeginTx(ctx, &sql.TxOptions{ReadOnly: false})
	defer trDB.Rollback()
	if err != nil {
		return myerrors.ErrBadDBTransaction
	}
	var balance float64
	row := trDB.QueryRowContext(ctx, "SELECT balance FROM account WHERE accountID = $1 FOR UPDATE", tr.AccountID)
	err = row.Scan(&balance)
	if err != nil {
		return errors.Wrap(err, "Coundn't get balance")
	}
	if balance < math.Abs(tr.Amount) {
		return fmt.Errorf("bad balance")
	}
	res, err := trDB.ExecContext(ctx, "UPDATE account SET balance = $1 WHERE accountID = $2", tr.Amount+balance, tr.AccountID)
	numRows, errExistRowsAffected := res.RowsAffected()
	if err != nil || (errExistRowsAffected == nil && numRows == 0) {
		return errors.Wrap(err, "DB error: can't update account balance")
	}

	res, err = trDB.ExecContext(ctx, "INSERT INTO transaction (transactionID, amount, timestamp, accountID)  VALUES ($1, $2, $3, $4)", tr.TransactionID, tr.Amount, time.Now().Unix(), tr.AccountID)
	numRows, errExistRowsAffected = res.RowsAffected()
	if err != nil || (errExistRowsAffected == nil && numRows == 0) {
		return errors.Wrap(err, "DB error: can't insert transaction")
	}
	trDB.Commit()
	return nil
}

func (repo *transactionRepo) PutMoney(ctx context.Context, tr trans.Transaction) error {
	trDB, err := repo.dbTrans.BeginTx(ctx, &sql.TxOptions{ReadOnly: false})
	defer trDB.Rollback()
	if err != nil {
		return myerrors.ErrBadDBTransaction
	}
	var balance float64
	row := trDB.QueryRowContext(ctx, "SELECT balance FROM account WHERE accountID = $1 FOR UPDATE", tr.AccountID)
	err = row.Scan(&balance)
	if err != nil {
		return errors.Wrap(err, "Coundn't get balance")
	}
	res, err := trDB.ExecContext(ctx, "UPDATE account SET balance = balance + $1 WHERE accountID = $2", tr.Amount+balance, tr.AccountID)
	numRows, errExistRowsAffected := res.RowsAffected()
	if err != nil || (errExistRowsAffected == nil && numRows == 0) {
		return errors.Wrap(err, "DB error: can't update account balance")
	}

	res, err = trDB.ExecContext(ctx, "INSERT INTO transaction (transactionID, amount, timestamp, accountID)  VALUES ($1, $2, $3, $4)", tr.TransactionID, tr.Amount, time.Now().Unix(), tr.AccountID)
	numRows, errExistRowsAffected = res.RowsAffected()
	if err != nil || (errExistRowsAffected == nil && numRows == 0) {
		return errors.Wrap(err, "DB error: can't insert transaction")
	}
	trDB.Commit()
	return nil
}
