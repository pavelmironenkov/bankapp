package delivery

import (
	"encoding/json"
	"io"
	"net/http"

	"go.uber.org/zap"

	"bankingapp/internal/myerrors"
	"bankingapp/internal/producer"
	"bankingapp/internal/producer/usecase"
)

type TransactionsHandler struct {
	TransUseCase usecase.TransactionsUseCase
	Logger       *zap.SugaredLogger
}

func (th *TransactionsHandler) ProduceTransaction(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant read request body")
		return
	}
	tr := &transactions.Transaction{}
	err = json.Unmarshal(body, tr)
	if err != nil {
		myerrors.JsonError(w, http.StatusBadRequest, "cant unpack payload")
		return
	}
	err = th.TransUseCase.ProduceTransaction(r.Context(), *tr)
	if err != nil {
		myerrors.JsonError(w, http.StatusInternalServerError, err.Error())
		return
	}
	w.WriteHeader(http.StatusOK)
}
