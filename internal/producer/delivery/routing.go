package delivery

import (
	"net/http"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"bankingapp/internal/middleware"
)

func RouterWithHandlers(transactionHandler TransactionsHandler) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/transaction", transactionHandler.ProduceTransaction).Methods("POST")
	return r
}

func PostProcessingRouter(r *mux.Router, logger *zap.SugaredLogger) http.Handler {
	r.Use(middleware.Auth)
	r.Use(middleware.AccessLog(logger))
	r.Use(middleware.Panic)
	return r
}
