package transactions

type Transaction struct {
	TransactionID    string
	Amount           float64
	Timestamp        uint32
	AccountID        string
}
