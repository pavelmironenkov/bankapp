package usecase

import (
	"bankingapp/internal/myerrors"
	trans "bankingapp/internal/producer"
	"bankingapp/pkg/token"
	"context"
	"encoding/json"

	"github.com/IBM/sarama"
	"github.com/hashicorp/go-uuid"
	"github.com/pkg/errors"
)

type TransactionsUseCase interface {
	ProduceTransaction(ctx context.Context, tr trans.Transaction) error
}

type Transactions struct {
	producer sarama.SyncProducer
}

func NewTransactionsUseCase(p sarama.SyncProducer) *Transactions {
	return &Transactions{producer: p}
}

func (t *Transactions) ProduceTransaction(ctx context.Context, tr trans.Transaction) error {
	account, err := token.UserFromContext(ctx)
	if err != nil || account.AccountID != tr.AccountID {
		return myerrors.ErrNoAuth
	}
	transID, err := uuid.GenerateUUID()
	if err != nil {
		return errors.Wrap(err, "error of generating uuid ")
	}
	tr.TransactionID = transID
	// Преобразование сообщения в JSON что бы потом отправить через kafka
	bytes, err := json.Marshal(tr)
	if err != nil {
		return err
	}

	msg := &sarama.ProducerMessage{
		Topic: "queue_transactions",
		Key:   sarama.StringEncoder(transID),
		Value: sarama.ByteEncoder(bytes),
	}

	_, _, err = t.producer.SendMessage(msg)
	return err
}

