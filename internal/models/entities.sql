CREATE EXTENSION "uuid-ossp";

CREATE TABLE IF NOT EXISTS usr(
    userID uuid primary key default uuid_generate_v4(),
    email  varchar(255) not null,
    name varchar(255) not null
); 

CREATE TABLE IF NOT EXISTS account(
    accountID uuid primary key default uuid_generate_v4(),
    balance double precision default 0.0 not null,
    username varchar(255) unique not null,
    password varchar(255) not null,
    userID uuid  not null,
    foreign key (userID) references usr(userID)
);

create table IF NOT EXISTS transaction(
    transactionID uuid primary key default uuid_generate_v4(),
    amount double precision not null,
    timestamp timestamp not null, 
    accountID uuid not null,
    foreign key (accountID) references account(accountID)
);
