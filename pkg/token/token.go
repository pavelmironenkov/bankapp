package token

import (
	"context"
	"errors"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type sessKey string

var Key interface{}

func init() {
	Key = []byte("osfhvjfblkvbke")
}

var (
	SessionKey   sessKey = "sessionKey"
	ErrNoAuth            = errors.New("no session found")
	ErrConvIface         = errors.New("hasn't convert interfaces")
)

type UserFromToken struct {
	UserID    string
	AccountID string
}

func UserFromContext(ctx context.Context) (*UserFromToken, error) {
	sess, ok := ctx.Value(SessionKey).(*UserFromToken)
	if !ok || sess == nil {
		return nil, ErrNoAuth
	}
	return sess, nil
}

func ContextWithUser(ctx context.Context, sess *UserFromToken) context.Context {
	return context.WithValue(ctx, SessionKey, sess)
}

func Create(account UserFromToken) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": map[string]interface{}{
			"accountid": account.AccountID,
			"userid":    account.UserID,
		},
		"iat": time.Now().Unix(),
		"exp": time.Now().Add(30 * 24 * time.Hour).Unix(),
	})
	return token.SignedString(Key)
}

func Check(tokenString string) (*UserFromToken, error) {
	_, tokenString, ok := strings.Cut(tokenString, "Bearer ")
	if !ok {
		return nil, ErrNoAuth
	}

	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(t *jwt.Token) (interface{}, error) { return Key, nil })
	if err != nil {
		return nil, ErrNoAuth
	}
	user, ok := (claims["user"]).(map[string]interface{})
	if !ok {
		return nil, ErrConvIface
	}
	userID, ok := user["userid"].(string)
	if !ok {
		return nil, ErrConvIface
	}

	accountID, ok := user["accountid"].(string)
	if !ok {
		return nil, ErrConvIface
	}

	return &UserFromToken{UserID: userID, AccountID: accountID}, nil

}
